// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class dragonSmugglersProtoTarget : TargetRules
{
	public dragonSmugglersProtoTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("dragonSmugglersProto");
	}
}
