// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "dragonSmugglersProtoGameMode.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodedragonSmugglersProtoGameMode() {}
// Cross Module References
	DRAGONSMUGGLERSPROTO_API UClass* Z_Construct_UClass_AdragonSmugglersProtoGameMode_NoRegister();
	DRAGONSMUGGLERSPROTO_API UClass* Z_Construct_UClass_AdragonSmugglersProtoGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_dragonSmugglersProto();
// End Cross Module References
	void AdragonSmugglersProtoGameMode::StaticRegisterNativesAdragonSmugglersProtoGameMode()
	{
	}
	UClass* Z_Construct_UClass_AdragonSmugglersProtoGameMode_NoRegister()
	{
		return AdragonSmugglersProtoGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AdragonSmugglersProtoGameMode()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameModeBase();
			Z_Construct_UPackage__Script_dragonSmugglersProto();
			OuterClass = AdragonSmugglersProtoGameMode::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20880288u;


				static TCppClassTypeInfo<TCppClassTypeTraits<AdragonSmugglersProtoGameMode> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("dragonSmugglersProtoGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("dragonSmugglersProtoGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AdragonSmugglersProtoGameMode, 3582495108);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AdragonSmugglersProtoGameMode(Z_Construct_UClass_AdragonSmugglersProtoGameMode, &AdragonSmugglersProtoGameMode::StaticClass, TEXT("/Script/dragonSmugglersProto"), TEXT("AdragonSmugglersProtoGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AdragonSmugglersProtoGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
