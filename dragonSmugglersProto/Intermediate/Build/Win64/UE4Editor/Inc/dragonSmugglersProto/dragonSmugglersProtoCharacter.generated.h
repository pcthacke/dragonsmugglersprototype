// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DRAGONSMUGGLERSPROTO_dragonSmugglersProtoCharacter_generated_h
#error "dragonSmugglersProtoCharacter.generated.h already included, missing '#pragma once' in dragonSmugglersProtoCharacter.h"
#endif
#define DRAGONSMUGGLERSPROTO_dragonSmugglersProtoCharacter_generated_h

#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_RPC_WRAPPERS
#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAdragonSmugglersProtoCharacter(); \
	friend DRAGONSMUGGLERSPROTO_API class UClass* Z_Construct_UClass_AdragonSmugglersProtoCharacter(); \
public: \
	DECLARE_CLASS(AdragonSmugglersProtoCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/dragonSmugglersProto"), NO_API) \
	DECLARE_SERIALIZER(AdragonSmugglersProtoCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAdragonSmugglersProtoCharacter(); \
	friend DRAGONSMUGGLERSPROTO_API class UClass* Z_Construct_UClass_AdragonSmugglersProtoCharacter(); \
public: \
	DECLARE_CLASS(AdragonSmugglersProtoCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/dragonSmugglersProto"), NO_API) \
	DECLARE_SERIALIZER(AdragonSmugglersProtoCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AdragonSmugglersProtoCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AdragonSmugglersProtoCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AdragonSmugglersProtoCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AdragonSmugglersProtoCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AdragonSmugglersProtoCharacter(AdragonSmugglersProtoCharacter&&); \
	NO_API AdragonSmugglersProtoCharacter(const AdragonSmugglersProtoCharacter&); \
public:


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AdragonSmugglersProtoCharacter(AdragonSmugglersProtoCharacter&&); \
	NO_API AdragonSmugglersProtoCharacter(const AdragonSmugglersProtoCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AdragonSmugglersProtoCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AdragonSmugglersProtoCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AdragonSmugglersProtoCharacter)


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AdragonSmugglersProtoCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AdragonSmugglersProtoCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AdragonSmugglersProtoCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AdragonSmugglersProtoCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AdragonSmugglersProtoCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AdragonSmugglersProtoCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AdragonSmugglersProtoCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AdragonSmugglersProtoCharacter, L_MotionController); }


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_11_PROLOG
#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_RPC_WRAPPERS \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_INCLASS \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_INCLASS_NO_PURE_DECLS \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
