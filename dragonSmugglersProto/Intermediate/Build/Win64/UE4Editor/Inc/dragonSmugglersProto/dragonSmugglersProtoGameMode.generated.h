// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DRAGONSMUGGLERSPROTO_dragonSmugglersProtoGameMode_generated_h
#error "dragonSmugglersProtoGameMode.generated.h already included, missing '#pragma once' in dragonSmugglersProtoGameMode.h"
#endif
#define DRAGONSMUGGLERSPROTO_dragonSmugglersProtoGameMode_generated_h

#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_RPC_WRAPPERS
#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAdragonSmugglersProtoGameMode(); \
	friend DRAGONSMUGGLERSPROTO_API class UClass* Z_Construct_UClass_AdragonSmugglersProtoGameMode(); \
public: \
	DECLARE_CLASS(AdragonSmugglersProtoGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/dragonSmugglersProto"), DRAGONSMUGGLERSPROTO_API) \
	DECLARE_SERIALIZER(AdragonSmugglersProtoGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAdragonSmugglersProtoGameMode(); \
	friend DRAGONSMUGGLERSPROTO_API class UClass* Z_Construct_UClass_AdragonSmugglersProtoGameMode(); \
public: \
	DECLARE_CLASS(AdragonSmugglersProtoGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/dragonSmugglersProto"), DRAGONSMUGGLERSPROTO_API) \
	DECLARE_SERIALIZER(AdragonSmugglersProtoGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DRAGONSMUGGLERSPROTO_API AdragonSmugglersProtoGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AdragonSmugglersProtoGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DRAGONSMUGGLERSPROTO_API, AdragonSmugglersProtoGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AdragonSmugglersProtoGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DRAGONSMUGGLERSPROTO_API AdragonSmugglersProtoGameMode(AdragonSmugglersProtoGameMode&&); \
	DRAGONSMUGGLERSPROTO_API AdragonSmugglersProtoGameMode(const AdragonSmugglersProtoGameMode&); \
public:


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DRAGONSMUGGLERSPROTO_API AdragonSmugglersProtoGameMode(AdragonSmugglersProtoGameMode&&); \
	DRAGONSMUGGLERSPROTO_API AdragonSmugglersProtoGameMode(const AdragonSmugglersProtoGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DRAGONSMUGGLERSPROTO_API, AdragonSmugglersProtoGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AdragonSmugglersProtoGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AdragonSmugglersProtoGameMode)


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_9_PROLOG
#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_RPC_WRAPPERS \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_INCLASS \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_INCLASS_NO_PURE_DECLS \
	dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID dragonSmugglersProto_Source_dragonSmugglersProto_dragonSmugglersProtoGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
